<?php

//
// POST THUMBNAIL
//
add_theme_support ( 'post-thumbnails' );

//
// MENU
//
add_theme_support ( 'menus' );

//
// EDITOR BUTTONS
//
require ('cooperfemsa-editor-buttons/cooperfemsa-buttons.php');

//
// REGISTER MENUS
//
function register_cooperfemsa_menus() {
	register_nav_menus ( array (
			'main-menu' => __ ( 'Menu Principal' ) 
	) );
}
add_action ( 'init', 'register_cooperfemsa_menus' );

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter ( 'ot_theme_mode', '__return_true' );

/**
 * Required: include OptionTree.
 */
require (trailingslashit ( get_template_directory () ) . 'option-tree/ot-loader.php');

//
// Remove Option Tree Settings Menu
//
// add_filter('ot_show_pages', '__return_false');

/**
 * Extended Walker class for use with the
 * Zurb Foundation toolkit Dropdown menus in Wordpress.
 * Edited to support n-levels submenu.
 * 
 * @author alamosaravali http://alamoweb.com.br
 * @license CC BY 4.0 https://creativecommons.org/licenses/by/4.0/
 *         
 */
class FoundationNavMenuWalker extends Walker_Nav_Menu {
	
	//
	// START LEVEL
	//
	function start_lvl(&$output, $depth = 0, $args = array()) {
		if ($depth == 0) {
			$output .= "\n<ul class=\"sub-menu flyout\">\n";
		}
		else if ($depth >= 1) {
			$output .= "\n<ul class=\"sub-sub-menu flyout\">\n";
		}
	}
	
	//
	// END LEVEL
	//
	function end_lvl(&$output, $depth = 0, $args = array()) {
		$output .= "\n</ul>\n";
	}
	
	//
	// START ELEMENT
	//
	function start_el(&$output, $item, $depth = 0, $args = array()) {
		$item_html = '';
		parent::start_el ( $item_html, $item, $depth, $args );
		
		$classes = empty ( $item->classes ) ? array () : ( array ) $item->classes;
		// if(in_array('has-flyout', $classes) && $depth == 0) {
		// $item_html = str_replace('</a>', '</a><a class="flyout-toggle" href="#"><span> </span></a>', $item_html);
		// }
		
// 		if (in_array ( 'has-flyout', $classes ) && $depth > 0) {
// 			$item_html = '<li class="divider"><a href="#">--------------------</a></li>' . $item_html;
// 		}
		
		$output .= $item_html;
	}
	
	//
	// END ELEMENT
	//
	function end_el(&$output, $item, $depth = 0, $args = array()) {
		$output .= "\n</li>\n";
	}
	
	//
	// DISPLAY ELEMENT
	//
	function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
		$element->has_children = ! empty ( $children_elements [$element->ID] );
		$element->classes [] = ($element->current || $element->current_item_ancestor) ? 'active' : '';
		$element->classes [] = ($element->has_children) ? 'has-flyout' : '';
	
		parent::display_element ( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}
