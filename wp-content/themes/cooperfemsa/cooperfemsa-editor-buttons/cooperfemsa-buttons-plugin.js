(function() {
    tinymce.create('tinymce.plugins.Cooperfemsa', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            //
            // COOPERFEMSA SLIDER
            //
            ed.addButton('cooperfemsa_slider', {
                title : 'Cooperfemsa Slider',
                cmd : 'cooperfemsa_slider',
                image : url + '/cooperfemsa_slider.png'
            });
            ed.addCommand('cooperfemsa_slider', function () {
                var text = '[cooperfemsa_slider columns="" width="" height="" marginright="" /]';
                ed.execCommand("mceInsertContent", 0, text);
            });

            //
            // COOPERFEMSA GRID
            //
            ed.addButton('cooperfemsa_grid', {
                title : 'Cooperfemsa Grid',
                cmd : 'cooperfemsa_grid',
                image : url + '/cooperfemsa_grid.png'
            });
            ed.addCommand('cooperfemsa_grid', function () {
                var selectedText = ed.selection.getContent();
                var text = '[cooperfemsa_grid columns=""]' + selectedText + '[/cooperfemsa_grid]';

                ed.execCommand("mceInsertContent", 0, text);
            });

            //
            // COOPERFEMSA CALL
            //
            ed.addButton('cooperfemsa_call', {
                title : 'Cooperfemsa Call',
                cmd : 'cooperfemsa_call',
                image : url + '/cooperfemsa_call.png'
            });
            ed.addCommand('cooperfemsa_call', function () {
                var text = '[cooperfemsa_call columns="" type="" slug="" marginright="" /]';

                ed.execCommand("mceInsertContent", 0, text);
            });
            
            //
            // COOPERFEMSA RECENT POSTS
            //
            ed.addButton('cooperfemsa_posts', {
                title : 'Cooperfemsa Posts',
                cmd : 'cooperfemsa_posts',
                image : url + '/cooperfemsa_posts.png'
            });
            ed.addCommand('cooperfemsa_posts', function () {
                var text = '[cooperfemsa_posts columns="" count="" /]';

                ed.execCommand("mceInsertContent", 0, text);
            });
            
            //
            // COOPERFEMSA ROW
            //
            ed.addButton('cooperfemsa_row', {
                title : 'Cooperfemsa Row',
                cmd : 'cooperfemsa_row',
                image : url + '/cooperfemsa_row.png'
            });
            ed.addCommand('cooperfemsa_row', function () {
            	var selectedText = ed.selection.getContent();
                var text = '[cooperfemsa_row]' + selectedText + '[/cooperfemsa_row]';

                ed.execCommand("mceInsertContent", 0, text);
            });
            
            //
            // COOPERFEMSA PARCERIAS
            //
            ed.addButton('cooperfemsa_parcerias', {
                title : 'Cooperfemsa Parcerias',
                cmd : 'cooperfemsa_parcerias',
                image : url + '/cooperfemsa_posts.png'
            });
            ed.addCommand('cooperfemsa_parcerias', function () {
                var text = '[cooperfemsa_parcerias /]';

                ed.execCommand("mceInsertContent", 0, text);
            });
            
            //
            // COOPERFEMSA SIMULADOR
            //
            ed.addButton('cooperfemsa_simulador', {
            	title: 'Cooperfemsa Simulador',
            	cmd: 'cooperfemsa_simulador',
            	image: url + '/cooperfemsa_slider.png',
            });
            ed.addCommand('cooperfemsa_simulador', function () {
            	var text = '[cooperfemsa_simulador columns="" /]';
            	
            	ed.execCommand('mceInsertContent', 0, text);
            });
        },

        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                longname : 'Cooperfemsa Buttons',
                author : 'Alamo Saravali',
                authorurl : 'http://alamoweb.com.br',
                infourl : 'http://alamoweb.com.br',
                version : "1.0"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add( 'cooperfemsa', tinymce.plugins.Cooperfemsa );
})();
