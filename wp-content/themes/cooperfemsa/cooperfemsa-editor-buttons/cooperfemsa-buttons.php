<?php
add_action( 'init', 'cooperfemsa_buttons' );
function cooperfemsa_buttons() {
    add_filter( "mce_external_plugins", "cooperfemsa_add_buttons" );
    add_filter( 'mce_buttons', 'cooperfemsa_register_buttons' );
}

function cooperfemsa_add_buttons( $plugin_array ) {
    $plugin_array['cooperfemsa'] = get_template_directory_uri() . '/cooperfemsa-editor-buttons/cooperfemsa-buttons-plugin.js';
    return $plugin_array;
}

function cooperfemsa_register_buttons( $buttons ) {
    array_push( $buttons, 'cooperfemsa_slider', 'cooperfemsa_call', 'cooperfemsa_posts', 'cooperfemsa_grid', 'cooperfemsa_row', 'cooperfemsa_parcerias', 'cooperfemsa_simulador' );
    return $buttons;
}

//
// SHORTCODE: SLIDE
// $atts, $content = ''
//
function cooperfemsa_slider_shortcode ($atts) {
    extract(shortcode_atts( array(
        	'width' => '100%',
        	'height' => '400px',
    		'columns' => 8,
    		'marginright' => '0px'
    ), $atts));
    
    if ($columns > 12) {
    	$columns = 12;
    }
    else if ($columns <= 0) {
    	$columns = 1;
    }
    
    $arrayColumns = array(
    		'1' => 'one',
    		'2' => 'two',
    		'3' => 'three',
    		'4' => 'four',
    		'5' => 'five',
    		'6' => 'six',
    		'7' => 'seven',
    		'8' => 'eight',
    		'9' => 'nine',
    		'10' => 'ten',
    		'11' => 'eleven',
    		'12' => 'twelve',
    );
    
    $columns = $arrayColumns[$columns];

	$sliderImages = ot_get_option('imagens', array());
	$html = '';

	if (count($sliderImages) > 0) {
		$id = 'cooperfemsa-carousel-' . rand(1, 100);
		$html .= '<div class="' . $columns . ' columns no-padding-important paddingright10px-important" style="margin-right: ' . $marginright . '">';
		$html .= '<div id="'.$id.'" class="cooperfemsa-slider" style="width: '.$width.'; height: '.$height.';">';

		//
		// SLIDES
		//
		for ($i = 0; $i < count($sliderImages); $i++) {
			$currentSlide = $sliderImages[$i];

			$html .= '<img src="'.$currentSlide['image'].'" alt="'.$currentSlide['title'].'" style="width: '.$width.'; height: '.$height.';">';
		}


		//
		// CONTROLS
		//

		$html .= '</div>';
		$html .= '</div>';
	}
	
	// add slider to JS Array
	echo '
			<script>
				slidersArray.push("' . $id . '");
			</script>
		 ';

	return $html;
}
add_shortcode('cooperfemsa_slider', 'cooperfemsa_slider_shortcode');



//
// SHORTCODE: POSTS
// $atts, $content = ''
//
function cooperfemsa_posts_shortcode ($atts) {
	extract(shortcode_atts( array(
			'count' => '4',
			'columns' => 12,
	), $atts));
	
	if ($columns > 12) {
		$columns = 12;
	}
	else if ($columns <= 0) {
		$columns = 1;
	}
	
	$arrayColumns = array(
			'1' => 'one',
			'2' => 'two',
			'3' => 'three',
			'4' => 'four',
			'5' => 'five',
			'6' => 'six',
			'7' => 'seven',
			'8' => 'eight',
			'9' => 'nine',
			'10' => 'ten',
			'11' => 'eleven',
			'12' => 'twelve',
	);
	
	$columns = $arrayColumns[$columns];
	
	// get recent posts
	$recentPosts = new WP_Query(array(
			'posts_per_page' => $count,
			'orderby' => 'date',
			'category_name' => 'artigos',
			'post_status' => 'publish',
			'order' => 'DESC',
	));
	
	// verify if has posts
	if ($recentPosts->have_posts()) {
		$html = '<div class="' . $columns . ' columns no-padding cooperfemsa-posts">';
		$html .= 	'<div class="twelve columns no-padding">';
		$url = get_template_directory_uri();
		$html .= 		'<img class="no-vertical-align" src="' . $url . '/img/cooperfemsa-posts-icon.png"> <span class="cooperfemsa-posts-title">Principais Destaques</span>';
		$html .= 	'</div>';
		while( $recentPosts->have_posts() ) {
			$recentPosts->the_post();
			$description = get_post_meta($recentPosts->post->ID, 'Descrição');
			
			$html .= '<div class="twelve columns no-padding-important cooperfemsa-a-post">';
			$html .= 	'<div class="cooperfemsa-a-post-thumbnail">';
			$html .= 		'<a class="no-style" href="' . get_permalink($recentPosts->post->ID) . '">' . get_the_post_thumbnail($recentPosts->post->ID, array(100,100)) . '</a>';
			$html .= 	'</div>';
			$html .= 	'<div class="cooperfemsa-a-post-content">';
			$html .= 		'<a class="no-style" href="' . get_permalink($recentPosts->post->ID) . '">';
			$html .= 			'<span class="cooperfemsa-a-post-title">' . get_the_title() . '</span>';
			$html .= 		'</a>';
			$html .= 		'<br>';
			$html .= 		'<a class="no-style" href="' . get_permalink($recentPosts->post->ID) . '">';
			$html .= 			'<span class="recent-post-description">' . $description[0] . '</span>';
			$html .= 		'</a>';
			$html .= 	'</div>';
			$html .= '</div>';
		}
		$html .= '</div>';
	}
	
	return $html;
}
add_shortcode('cooperfemsa_posts', 'cooperfemsa_posts_shortcode');


//
// SHORTCODE: CALL
// $atts, $content = ''
//
function cooperfemsa_call_shortcode ($atts) {
	extract(shortcode_atts( array(
			'slug' => null,
			'type' => 'post',
			'marginright' => '0px',
			'columns' => 4,
			'height' => null,
	), $atts));
	
	if ($height) {
		$height = 'height: ' . $height . ' !important;';
	}
	
	if ($columns > 12) {
		$columns = 12;
	}
	else if ($columns <= 0) {
		$columns = 1;
	}
	
	$arrayColumns = array(
			'1' => 'one',
			'2' => 'two',
			'3' => 'three',
			'4' => 'four',
			'5' => 'five',
			'6' => 'six',
			'7' => 'seven',
			'8' => 'eight',
			'9' => 'nine',
			'10' => 'ten',
			'11' => 'eleven',
			'12' => 'twelve',
	);
	
	$columns = $arrayColumns[$columns];
	
	$html = '';
	
	if ($slug) {
		$args = array(
				'name' => $slug,
				'post_type' => $type,
				'post_status' => 'publish',
				'posts_per_page' => 1,
				'caller_get_posts'=> 1
		);
		$my_query = null;
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) {
			$url = get_template_directory_uri();
			while ($my_query->have_posts()) {
				$my_query->the_post();
				$permaLink = get_permalink($my_query->post->ID);
				$description = get_post_meta($my_query->post->ID, 'Descrição');
				
				$html .= '<div class="' . $columns . ' columns no-padding" style="margin-right: ' . $marginright . '; padding-left: 3px; padding-right: 3px;">';
				$html .= 	'<div class="cooperfemsa-call" style="' . $height . '">';
				$html .= 		'<img class="no-vertical-align" src="' . $url . '/img/cooperfemsa-posts-icon.png"> <span class="cooperfemsa-posts-title">' . get_the_title() . '</span>';
				$html .= 		'<br>';
				$html .= 		'<br>';
				$html .= 		'<center> <a href="' . $permaLink . '" class="no-style">' . get_the_post_thumbnail() . '</a></center>';
				$html .= 		'<br>';
				$html .= 		$description[0];
				$html .= 		'<div class="clearfix"></div>';
				$html .= 		'<a class="no-style" href="' . $permaLink . '"><img class="cooperfemsa-plus-icon" src="' . $url . '/img/plus_button.png"></a>';
				$html .= 		'<div class="clearfix"></div>';
				$html .= 	'</div>';
				$html .= '</div>';
			}
		}
		wp_reset_query();  // Restore global post data stomped by the_post().
	}
	
	return $html;
}
add_shortcode('cooperfemsa_call', 'cooperfemsa_call_shortcode');



//
// SHORTCODE: GRID
// $atts, $content = ''
//
function cooperfemsa_grid_shortcode ($atts, $content = '') {
	extract(shortcode_atts( array(
		'columns' => '12',
	), $atts));
	
	// smartphone
	if (!is_numeric($columns) || $columns > 12) {
		$columns = 12;
	}
	else if ($columns < 1) {
		$columns = 1;
	}
	
	$columns .= "";
	
	$arrayStrings = array(
		"1" => "one",
		"2" => "two",
		"3" => "three",
		"4" => "four",
		"5" => "five",
		"6" => "six",
		"7" => "seven",
		"8" => "eight",
		"9" => "nine",
		"10" => "ten",
		"11" => "eleven",
		"12" => "twelve",
	);
	
	$columns = $arrayStrings[$columns];
	
	$html = '<div class="' . $columns . ' columns">';
	$html .= do_shortcode($content);
	$html .= '</div>';
	
	return $html;
}
add_shortcode('cooperfemsa_grid', 'cooperfemsa_grid_shortcode');



//
// SHORTCODE: ROW
// $atts, $content = ""
//
function cooperfemsa_row_shortcode ($atts, $content = "") {
	$html = '<div class="row">';
	$html .= do_shortcode($content);
	$html .= '</div>';
	
	return $html;
}
add_shortcode('cooperfemsa_row', 'cooperfemsa_row_shortcode');



//
// SHORTCODE: PARCERIAS
//
function cooperfemsa_parcerias_shortcode ($atts, $content = "") {
	$html = '';
	
	$args=array(
			'category_name' => 'parcerias',
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'ASC',
	);
	$my_query = null;
	$my_query = new WP_Query($args);
	if( $my_query->have_posts() ) {
		while ($my_query->have_posts()) {
			$my_query->the_post();
			$link = get_post_meta($my_query->post->ID, 'Link');

			$html .= '<div class="cooperfemsa-a-parceria">';
			$html .= 	'<div class="row">';
			$html .= 		'<div class="three columns cooperfemsa-a-parceria-thumbnail" align="center">';
			$html .= 			get_the_post_thumbnail();
			$html .= 		'</div>';
			$html .= 		'<div class="nine columns">';
			$html .= 			get_the_content();
			$html .= 		'</div>';
			$html .= 	'</div>';
			$html .= 	'<div class="cooperfemsa-a-parceria-link">';
			$html .= 		'<a href="' . $link[0] . '" target="_blank"><img src="' . get_template_directory_uri() . '/img/plus_button.png"></a>';
			$html .= 	'</div>';
			$html .= 	'<div class="clearfix"></div>';
			$html .= '</div>';
		}
	}
	wp_reset_query();  // Restore global post data stomped by the_post().
	
	return $html;
}
add_shortcode('cooperfemsa_parcerias', 'cooperfemsa_parcerias_shortcode');


//
// SHORTCODE: SIMULADOR
//
function cooperfemsa_simulador_shortcode ($atts, $content = '') {
	$html = '';
	
	extract(shortcode_atts( array(
			'columns' => 12,
	), $atts));
	
	// smartphone
	if (!is_numeric($columns) || $columns > 12) {
		$columns = 12;
	}
	else if ($columns < 1) {
		$columns = 1;
	}
	
	$columns .= "";
	
	$arrayStrings = array(
			"1" => "one",
			"2" => "two",
			"3" => "three",
			"4" => "four",
			"5" => "five",
			"6" => "six",
			"7" => "seven",
			"8" => "eight",
			"9" => "nine",
			"10" => "ten",
			"11" => "eleven",
			"12" => "twelve",
	);
	
	$columns = $arrayStrings[$columns];
	
	//Parâmetros
	$linha_credito = array(
			array(
					'nome' => 'Crédito Pessoal',
					'taxa' => array(
							6 => 1.49,
							12 => 1.89,
							24 => 1.99,
							36 => 2.19,
					),
			),
			array(
					'nome' => 'Crédito eletro-eletrônico',
					'taxa' => array(
							6 => 1.69,
							12 => 1.89,
							24 => 1.99,
					),
			),
			array(
					'nome' => 'Crédito carros e motos',
					'taxa' => array(
							36 => 1.6,
					),
			),
			array(
					'nome' => 'Crédito material de construção',
					'taxa' => array(
							24 => 1.89,
					),
			),
			array(
					'nome' => 'Crédito viagens e turismo',
					'taxa' => array(
							24 => 1.59,
					),
			),
			array(
					'nome' => 'Crédito ação judicial',
					'taxa' => array(
							12 => 1.59,
					),
			),
			array(
					'nome' => 'Crédito educação',
					'taxa' => array(
							12 => 1.29,
							24 => 1.45,
							36 => 1.60,
					),
			),
			array(
					'nome' => 'Crédito autoestima',
					'taxa' => array(
							24 => 1.89,
					),
			),
			array(
					'nome' => 'IPTU/IPVA',
					'taxa' => array(
							10 => 1.29,
					),
			),
	);
	
	//$seguro = 1.0165;
	$seguro = 1;

	$id = 'cooperfemsa-simulador-' . rand(1, 100);
	$url = get_template_directory_uri();
	
	$html .= '<div class="' . $columns . ' columns no-padding" style="margin-right: ' . $marginright . '; padding-left: 3px; padding-right: 3px;">';
	$html .= 	'<div class="cooperfemsa-call">';
	$html .= 		'<img class="no-vertical-align" src="' . $url . '/img/cooperfemsa-posts-icon.png"> <span class="cooperfemsa-posts-title">Simulador de Crédito</span>';
	$html .= 		'<br>';
	$html .= 		'<br>';
	$html .= 		'<form id="' . $id . '" action="javascript:execSimulador(\'' . $id . '\');" method="post">';
	$html .= 			'<select name="chv_linha_credito" class="chv_linha_credito" onchange="change_linha(\'' . $id . '\');">';
	
	foreach($linha_credito as $key => $value) {
		$html .= 			'<option value="' . $key . '">' . $value['nome'] . '</option>';
	}
	
	$html .=			'</select><br><br>';
	
	$html .=			'<div class="resultado-linhacredito" style="display: none;">';
	$html .=				'<strong>Parcelamento <span class="name"></span></strong>';
	
	$html .=				'<div class="resultado-porcentagens"></div>';
	
	$html .=				'<br>**Em caso de refinanciamento consulte a taxa na CooperFemsa.<br><br>';
	
	$html .=				'<strong>Valor desejado:</strong><br><br>';
	$html .=				'R$&nbsp;<input name="vlr_desejado" type="text" class="vlr_desejado" value="" size="10" style="display: inline; width: auto; height: 30px;" />';
	$html .=				'&nbsp;<button type="button" onclick="change_valor(\'' . $id . '\');">OK</button>';
	
	$html .=					'<div class="resultado-valordesejado" style="display: none;">';
	$html .=						'<br><strong>Opções de parcelamento:</strong><br>';
	$html .=						'<div class="opcoes-parcelamento"></div>';
	$html .=					'</div>';
	$html .=			'</div>';
	$html .= 		'</form>';
	$html .= 	'</div>';
	$html .= '</div>';
	
	return $html;
}
add_shortcode('cooperfemsa_simulador', 'cooperfemsa_simulador_shortcode');