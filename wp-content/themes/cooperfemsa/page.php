<?php get_header(); ?>

<?php
$current = get_post();
echo apply_filters( 'the_content', $current->post_content );
?>

<?php get_footer(); ?>
