﻿<meta charset="UTF-8">
<style type="text/css">
	.simulador {
		font-size: 11px;
		font-family: Verdana,Arial,Helvetica,Sans-Serif;
		font-style: normal;
		font-weight: normal;
	}
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<?php
//Parâmetros
$linha_credito[1]['nome']='Escolha o Financiamento';
$linha_credito[2]['nome']='Crédito Pessoal';
$linha_credito[2]['taxa'][6]=1.49;
$linha_credito[2]['taxa'][12]=1.89;
$linha_credito[2]['taxa'][24]=1.99;
$linha_credito[2]['taxa'][36]=2.19;
$linha_credito[3]['nome']='Crédito eletroeletrônico';
$linha_credito[3]['taxa'][6]=1.49;
$linha_credito[3]['taxa'][12]=1.89;
$linha_credito[3]['taxa'][24]=1.99;
$linha_credito[4]['nome']='Crédito carros e motos';
$linha_credito[4]['taxa'][36]=1.6;
$linha_credito[5]['nome']='Crédito material de construção';
$linha_credito[5]['taxa'][24]=1.89;
$linha_credito[6]['nome']='Crédito viagens e turismo';
$linha_credito[6]['taxa'][24]=1.59;
$linha_credito[7]['nome']='Crédito ação judicial';
$linha_credito[7]['taxa'][12]=1.59;
$linha_credito[8]['nome']='Crédito educação';
$linha_credito[8]['taxa'][12]=1.29;
$linha_credito[8]['taxa'][24]=1.45;
$linha_credito[8]['taxa'][36]=1.60;
$linha_credito[9]['nome']='Crédito autoestima';
$linha_credito[9]['taxa'][24]=1.89;
$linha_credito[10]['nome']='IPTU/IPVA';
$linha_credito[10]['taxa'][10]=1.29;

//$seguro = 1.0165;
$seguro = 1;
?>

<div class="art-post simulador">
	<h3>Simulador de Empréstimos</h3>

	<strong>Linha de crédito:</strong>
	<form id="form_linha_credito" action="" method="post">
		<select name="chv_linha_credito" id="chv_linha_credito" onchange="change_linha()">
			<option value="">Selecione</option>

			<?php foreach($linha_credito as $key => $value) { ?>

				<option value="<?php echo $key; ?>" <?php if(isset($_POST['chv_linha_credito']) && $_POST['chv_linha_credito']==$key) { echo 'selected="selected"'; } ?>><?php echo $value['nome']; ?></option>

			<?php } ?>

		</select>
		<br>
		<br>

		<?php if (isset($_POST['chv_linha_credito']) && $_POST['chv_linha_credito']) { ?>

			<strong>Parcelamento
			<?php echo $linha_credito[$_POST['chv_linha_credito']]['nome']; ?>:</strong><br />

			<?
			$prazo = 1;
			foreach($linha_credito[$_POST['chv_linha_credito']]['taxa'] as $key => $value) {
				echo str_pad($prazo,2,'0',STR_PAD_LEFT).' à '.str_pad($key,2,'0',STR_PAD_LEFT).' parcelas '.number_format($value,2,',','.').'% a.m. <Br>';
				$prazo = $key + 1;
			}

			if(isset($_POST['vlr_desejado']) && $_POST['vlr_desejado'])
				$_POST['vlr_desejado'] = str_replace(array('.',','),array('','.'),$_POST['vlr_desejado']);

			?>
			<br />
			**Em caso de refinanciamento consulte a taxa na CooperFemsa.
			<br />
			<br />

			<strong>Valor desejado:</strong> <br />
			R$&nbsp;<input name="vlr_desejado" type="text" id="vlr_desejado" value="<?php if(isset($_POST['vlr_desejado']) && $_POST['vlr_desejado']) { echo number_format($_POST['vlr_desejado'],2,',','.'); } ?>" size="10" />
			<button type="button" onclick="change_valor();">OK</button>

		<?php } ?>

		<?php
		if (isset($_POST['chv_linha_credito']) && $_POST['chv_linha_credito'] && isset($_POST['vlr_desejado']) && $_POST['vlr_desejado']) {

			$vlr_solicitado = $_POST['vlr_desejado'];
			$vlr_solicitado = $vlr_solicitado * $seguro;
			?>
			<br>
			<br>
			<strong>Opções de parcelamento:</strong><br />
			<div id="prazo">
				<?php
				$prazo = 1;
				
				foreach($linha_credito[$_POST['chv_linha_credito']]['taxa'] as $key => $value) {
					while($prazo<=$key) {
						$juros = $value / 100;
						$vlr_parcela=-(($juros*$vlr_solicitado)/((pow((1+$juros),($prazo) * -1))-1));
						$vlr_parcela = $vlr_parcela;
						?>
						<?php echo str_pad($prazo,2,'0',STR_PAD_LEFT); ?>x de R$ <?php echo number_format($vlr_parcela,2,',','.'); ?> (<?php echo number_format($value,2,',','.'); ?>% a.m.)<br />
						<?php
						$prazo++;
					}
					$prazo = $key + 1;
				}
				?>
			</div>
		<?php } ?>
	</form>
</div>
<script>
function change_linha() {
	$('#form_linha_credito').submit();
}

function change_valor() {
	$('#form_linha_credito').submit();
}

function change_prazo(prazo) {
	var chv_linha_credito = $('#chv_linha_credito option:selected').val();
	var vlr_desejado = $('#vlr_desejado').val();
	$.post('/simulador.php',{chv_linha_credito: chv_linha_credito, vlr_desejado: vlr_desejado, prazo: prazo},function(data) {
		$('#simulador').html(data);
	});	
}
</script>