//
// HELPERS
//

// VALID IS EMPTY STRING
function isEmpty (text) {
	if (text === null || text === "" || text.length == 0) {
		// yes, it is empty
		return true;
	}
	else {
		// no, it is not empty
		return false;
	}
}

//VALID IS EMPTY LIST STRING
function isEmptyList (list) {
	var hasEmpty = false;
	
	for (var i in list) {
		var text = list[i];
		
		if (isEmpty(text)) {
			hasEmpty = true;
		}
	}
	
	return hasEmpty;
}

// VALID EMAIL
function isValidEmail (email) {
	var emailRegex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	return emailRegex.test(email);
}


//
// CONTACT US
//

// SUBMIT CONTACT US
function submitContactUs () {
	// get all inputs
	var inputs = {
			name: document.getElementById('cooperfemsa-contact-name').value,
			email: document.getElementById('cooperfemsa-contact-email').value,
			phone: document.getElementById('cooperfemsa-contact-phone').value,
			message: document.getElementById('cooperfemsa-contact-message').value,
	};
	
	if (isEmptyList(inputs)) {
		alert("Por favor, preencha todos os campos.");
	}
	else if (!isValidEmail(inputs.email)) {
		alert("Por favor, insira um email válido.");
	}
	else {
		// OK
		
		// SEND BY AJAX
		$.ajax({
			url: templateDirectory + "/ajax/sendContactUs.php",
			method: "POST",
			data: inputs,
			success: function (response) {
				response = $.parseJSON(response);
				
				if (response.status == 1) {
					alert("Email enviado com sucesso!\nEm breve, entraremos em contato.");
					
					// CLEAN FIELDS
					for (var i in inputs) {
						var id = "cooperfemsa-contact-" + i;
						
						document.getElementById(id).value = "";
					}
				}
				else {
					alert(response.message);
				}
			},
			error: function () {
				alert("Ops! Ocorreu um erro durante o envio do e-mail. Por favor, tente novamente.");
			}
		});
	}
}

//
// SIMULADOR
//
function execSimulador(elementId) {
	// get values
	var chv_linha_credito = $("#" + elementId + " .chv_linha_credito option:selected").val();
	var vlr_desejado = $("#" + elementId + " .vlr_desejado").val();
	
	// format data
	var data = {};
	
	if (chv_linha_credito !== null && chv_linha_credito != "") {
		data["chv_linha_credito"] = chv_linha_credito;
		
		if (vlr_desejado !== null && vlr_desejado !== "" && vlr_desejado !== "0" && vlr_desejado !== 0) {
			data["vlr_desejado"] = vlr_desejado;
		}
	}
	else {
		alert('Por favor, selecione uma linha de crédito antes.');
	}
	
	// make post
	$.post(templateDirectory + "/ajax/simulador.php", data, function (response) {
		// parse JSON to JavaScript Object
		response = $.parseJSON(response);
		
		if (response) {
			if (typeof(response.linha_credito) != "undefined") {
				$('#' + elementId + " .name").html(response.linha_credito.name);
				
				var porcentagensHtml = '';
				for (var i in response.linha_credito.options) {
					var option = response.linha_credito.options[i];
					
					porcentagensHtml += option + "<br>";
				}
				
				// change resultado-porcentagens
				$("#" + elementId + " .resultado-porcentagens").html(porcentagensHtml);
				
				// show linha-credito
				$("#" + elementId + " .resultado-linhacredito").show();
				
				if (typeof(response.valor_desejado) != "undefined") {
					var valorDesejadoHtml = '';
					for (var i in response.valor_desejado) {
						valorDesejadoHtml += response.valor_desejado[i] + "<br>";
					}
					
					// change resultado-valordesejado
					$("#" + elementId + " .opcoes-parcelamento").html(valorDesejadoHtml);
					
					// show valor desejado
					$("#" + elementId + " .resultado-valordesejado").show();
				}
			}
		}
	});
}

function change_linha (elementId) {
	
	$("#" + elementId + " .name").html('');
	
	$("#" + elementId + " .resultado-linhacredito").hide();
	
	$("#" + elementId + " .vlr_desejado").val('');
	
	$("#" + elementId + " .resultado-porcentagens").html('');
	
	$("#" + elementId + " .resultado-valordesejado").hide();
	
	$("#" + elementId + " .opcoes-parcelamento").html('');
	
	execSimulador(elementId);
}

function change_valor (elementId) {
	
	$("#" + elementId + " .resultado-valordesejado").hide();
	
	$("#" + elementId + " .opcoes-parcelamento").html('');
	
	execSimulador(elementId);
}