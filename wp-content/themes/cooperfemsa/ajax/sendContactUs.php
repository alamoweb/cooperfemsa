<?php
// get inputs
$inputs = $_POST;

// verify if has an empty input
$hasEmpty = false;
foreach ($inputs as $input) {
	if (!$input) {
		$hasEmpty = true;
	}
}

if ($hasEmpty) {
	echo json_encode(array(
			'status' => 0,
			'message' => 'Por favor, preencha todos os campos.',
	));
	die;
}
else {
	require 'PHPMailerAutoload.php';
	
	$mail = new PHPMailer;
	
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.cooperfemsa.com.br';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'sender@cooperfemsa.com.br';                 // SMTP username
	$mail->Password = 'secret';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to
	
	$mail->setFrom('sender@cooperfemsa.com.br', 'Mailer');
	$mail->addAddress('contato@cooperfemsa.com.br', 'COOPERFEMSA');     // Add a recipient
	$mail->addReplyTo($inputs['email'], $inputs['name']);
	
	$mail->isHTML(true);                                  // Set email format to HTML
	
	$mail->Subject = 'CONTATO VIA SITE - ' . $inputs['name'] . '<br>';
	
	$mail->Body    = '<strong>Nome:</strong> ' . $inputs['name'] . '<br>';
	$mail->Body    .= '<strong>Email:</strong> ' . $inputs['email'] . '<br>';
	$mail->Body    .= '<strong>Telefone:</strong> ' . $inputs['phone'] . '<br>';
	$mail->Body    .= '<strong>Mensagem:</strong><br>' . $inputs['message'];
	
	$mail->AltBody = 'Nome: ' . $inputs['name'] . '\n';
	$mail->AltBody .= 'Email: ' . $inputs['email'] . '\n';
	$mail->AltBody .= 'Telefone: ' . $inputs['phone'] . '\n';
	$mail->AltBody .= 'Mensagem:\n' . $inputs['message'];
	
	if(!$mail->send()) {
		echo json_encode(array(
				'status' => 0,
				'message' => 'Ops! Ocorreu um erro durante o envio do e-mail. Por favor, tente novamente.',
		));
		die;
	} else {
		echo json_encode(array(
				'status' => 1,
		));
		die;
	}
}