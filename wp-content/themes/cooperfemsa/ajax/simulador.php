<?php

// Parâmetros
$linha_credito = array (
		array (
				'nome' => 'Crédito Pessoal',
				'taxa' => array (
						6 => 1.49,
						12 => 1.89,
						24 => 1.99,
						36 => 2.19 
				) 
		),
		array (
				'nome' => 'Crédito eletro-eletrônico',
				'taxa' => array (
						6 => 1.69,
						12 => 1.89,
						24 => 1.99 
				) 
		),
		array (
				'nome' => 'Crédito carros e motos',
				'taxa' => array (
						36 => 1.6 
				) 
		),
		array (
				'nome' => 'Crédito material de construção',
				'taxa' => array (
						24 => 1.89 
				) 
		),
		array (
				'nome' => 'Crédito viagens e turismo',
				'taxa' => array (
						24 => 1.59 
				) 
		),
		array (
				'nome' => 'Crédito ação judicial',
				'taxa' => array (
						12 => 1.59 
				) 
		),
		array (
				'nome' => 'Crédito educação',
				'taxa' => array (
						12 => 1.29,
						24 => 1.45,
						36 => 1.60 
				) 
		),
		array (
				'nome' => 'Crédito autoestima',
				'taxa' => array (
						24 => 1.89 
				) 
		),
		array (
				'nome' => 'IPTU/IPVA',
				'taxa' => array (
						10 => 1.29 
				) 
		) 
);

// $seguro = 1.0165;
$seguro = 1;

$response = array ();

// chv_linha_credito
if (isset ( $_POST ['chv_linha_credito'] ) && $_POST ['chv_linha_credito']) {
	$response ['linha_credito'] = array();
	$response ['linha_credito']['name'] = $linha_credito [$_POST ['chv_linha_credito']] ['name'];
	$response ['linha_credito']['options'] = array();
	
	$prazo = 1;
	foreach($linha_credito[$_POST['chv_linha_credito']]['taxa'] as $key => $value) {
		$response ['linha_credito']['options'][] = str_pad($prazo,2,'0',STR_PAD_LEFT).' à '.str_pad($key,2,'0',STR_PAD_LEFT).' parcelas '.number_format($value,2,',','.').'% a.m. <Br>';
		$prazo = $key + 1;
	}
	
	// vlr_desejado
	if (isset ( $_POST ['vlr_desejado'] ) && $_POST ['vlr_desejado']) {
		$response ['valor_desejado'] = array ();
		$_POST['vlr_desejado'] = str_replace(array('.',','),array('','.'),$_POST['vlr_desejado']);
		$vlr_solicitado = $_POST ['vlr_desejado'];
		$vlr_solicitado = $vlr_solicitado * $seguro;
		
		$prazo = 1;
		
		foreach ( $linha_credito [$_POST ['chv_linha_credito']] ['taxa'] as $key => $value ) {
			while ( $prazo <= $key ) {
				$juros = $value / 100;
				$vlr_parcela = - (($juros * $vlr_solicitado) / ((pow ( (1 + $juros), ($prazo) * - 1 )) - 1));
				$vlr_parcela = $vlr_parcela;
				$response ['valor_desejado'] [] = str_pad ( $prazo, 2, '0', STR_PAD_LEFT ) . "x de R$ " . number_format ( $vlr_parcela, 2, ',', '.' ) . " (" . number_format ( $value, 2, ',', '.' ) . "% a.m.)";
				$prazo ++;
			}
			$prazo = $key + 1;
		}
	}
}

echo json_encode ( $response );
die ();