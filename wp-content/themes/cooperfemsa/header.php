<?php
header ( 'Content-Type: text/html; charset=utf-8' );
?>

<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->

		<html class="no-js" lang="en"> <!--<![endif]-->

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="<?php bloginfo('description'); ?>">
<title><?php if(!is_single()){ bloginfo('name'); } else { wp_title(); }  ?></title>

<!-- CSS -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all"
	href="<?php bloginfo( 'stylesheet_url' ); ?>">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- Included CSS Files (Compressed) -->
<link rel="stylesheet"
	href="<?php bloginfo('template_directory'); ?>/css/foundation.min.css">
<link rel="stylesheet"
	href="<?php bloginfo('template_directory'); ?>/css/app.css">
<script
	src="<?php bloginfo('template_directory'); ?>/js/modernizr.foundation.js"></script>

    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>
    
    <script>
	var slidersArray = new Array();
	var templateDirectory = "<?php bloginfo('template_directory'); ?>";
    </script>
</head>
<body>

	<!-- WRAPPER -->
	<div id="wrapper" class="row">

		<!-- HEADER -->
		<header class="twelve columns">
			<div class="row">
				<!-- LOGO -->
				<div id="logo" class="six mobile-four columns">
					<a href="<?php bloginfo("url"); ?>">
	                        <?php
							// get logo
							$logo = ot_get_option ( 'logo', false );
							?>
	                        <?php if (isset($logo) && $logo): ?>
	                            <img src="<?php echo $logo; ?>" alt="<?php bloginfo("name"); ?>" />
	                        <?php else: ?>
	                            <h1><?php bloginfo("name"); ?></h1>
	                        <?php endif; ?>
	               	</a>
				</div>
	
				<div class="three mobile-two columns paddingtop25px-important">
					<a id="associe-link" class="no-style"
						href="<?php echo ot_get_option('associe_se_link', '#'); ?>">>>>
						ASSOCIE-SE</a>
					<br>
					<br>
				</div>
	
				<div id="bloco-acesso-redes" class="three mobile-two columns right no-padding">
					<!-- INTERNET BANKING -->
					<div id="internet-banking-access" 
						class="twelve columns">
						<a href="<?php echo ot_get_option('internet_banking_url', '#'); ?>">
							<img
							src="<?php bloginfo('template_directory'); ?>/img/IB_BUTTON.png"
							alt="Internet Banking" />
						</a>
					</div>
	
					<!-- SOCIAL MIDIA -->
					<div id="social-midia"
						class="twelve columns">
	                        <?php
							//
							// GET SOCIAL MIDIA LINKS
							//
							$arraySocialMidia = array (
									'facebook' => array (
										'name' => 'Facebook',
										'link' => ot_get_option ( 'facebook', false ),
										'image' => ot_get_option ( 'facebook_icon', false ) 
									),
									'twitter' => array (
										'name' => 'Twitter',
										'link' => ot_get_option ( 'twitter', false ),
										'image' => ot_get_option ( 'twitter_icon', false ) 
									),
									'flickr' => array (
										'name' => 'Flickr',
										'link' => ot_get_option ( 'flickr', false ),
										'image' => ot_get_option ( 'flickr_icon', false ) 
									) 
							);
							?>
	                        <ul class="list-inline">
	                            <?php foreach ($arraySocialMidia as $socialMidia): ?>
	                                <li><a href="<?php echo $socialMidia['link']; ?>"
								target="_blank" title="<?php echo $socialMidia['name']; ?>"><img
									src="<?php echo $socialMidia['image']; ?>"
									alt="<?php echo $socialMidia['name']; ?>" /></a></li>
	                            <?php endforeach; ?>
	                        </ul>
					</div>
				</div>
			</div>
		</header>

		<!-- MENU -->
		<div class="twelve columns">
			<div class="row">
				<nav id="main-menu" class="twelve columns">
					<!-- TODO: MAKE FOUNDATION MENU -->
					<?php
					$args = array (
						'theme_location' => 'main-menu',
						'container' => false,             // remove menu container
				        'container_class' => '',          // class of container
				        'menu' => '',                     // menu name
				        'menu_class' => 'nav-bar',        // adding custom nav class
				        'before' => '',                   // before each link <a>
				        'after' => '',                    // after each link </a>
				        'link_before' => '',              // before each link text
				        'link_after' => '',               // after each link text
				        'depth' => 0,                     // limit the depth of the nav
				    	'fallback_cb' => false,			  // fallback function (see below)
						'walker' => new FoundationNavMenuWalker () 
					);
					wp_nav_menu ( $args );
					?>
				</nav>
			</div>
		</div>

		<!-- CONTAINER -->
		<div id="content">
