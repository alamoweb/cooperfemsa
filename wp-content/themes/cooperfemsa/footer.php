
</div>
<footer class="twelve columns">
	<div class="row">

		<!-- FOOTER LOGO -->
		<div id="footer-logo" class="four mobile-four columns center">
			<img src="<?php bloginfo('template_directory'); ?>/img/LOGO_back.png">
			<br> <span>#JuntosSomosMaisFortes</span>
		</div>

		<!-- CONTACT-US -->
		<div id="contact-us" class="four mobile-four columns">
			<img class="no-vertical-align"
				src="<?php bloginfo('template_directory'); ?>/img/cooperfemsa-posts-icon.png">
			<span class="cooperfemsa-posts-title">Fale Conosco</span>

			<form id="contact-us-form" action="javascript:submitContactUs();"
				method="post">
				<div class="row collapse">
					<div class="two mobile-one columns">
						<span class="prefix"><i class="fa fa-user fa-1"></i></span>
					</div>
					<div class="ten mobile-three columns">
						<input id="cooperfemsa-contact-name" type="text" name="name"
							placeholder="Nome">
					</div>
				</div>
				<div class="row collapse">
					<div class="two mobile-one columns">
						<span class="prefix"><i class="fa fa-at fa-1"></i></span>
					</div>
					<div class="ten mobile-three columns">
						<input id="cooperfemsa-contact-email" type="email" name="email"
							placeholder="Email">
					</div>
				</div>
				<div class="row collapse">
					<div class="two mobile-one columns">
						<span class="prefix"><i class="fa fa-phone fa-1"></i></span>
					</div>
					<div class="ten mobile-three columns">
						<input id="cooperfemsa-contact-phone" type="tel" name="phone"
							placeholder="Telefone">
					</div>
				</div>
				<div class="row collapse">
					<div class="two mobile-one columns" style="height: 120px;">
						<span class="prefix paddingtop40px-important"><i
							class="fa fa-envelope fa-1"></i></span>
					</div>
					<div class="ten mobile-three columns">
						<textarea id="cooperfemsa-contact-message" type="text"
							name="message" placeholder="Mensagem"></textarea>
					</div>
				</div>
				<div class="row collapse">
					<div class="twelve mobile-four columns">
						<input type="submit" value="Enviar">
					</div>
				</div>
			</form>
		</div>

		<!-- ABOUT -->
		<div id="about" class="four mobile-four columns">
			<div class="about-info">
				<div class="title">Sede São Paulo/SP</div>
				<br>
				<div class="description">
					Telefones: <a class="no-style" href="tel:01121025747">(11) 2102
						5747</a> / <a class="no-style" href="tel:01121025748">2102 5748</a>
					/ <a class="no-style" href="tel:01121025541">2102 5541</a> / <a
						class="no-style" href="tel:01121026492">2102 6492</a>
				</div>
			</div>
			<div class="about-info">
				<div class="title">PAC Belo Horizonte/MG</div>
				<br>
				<div class="description">
					Telefones: <a class="no-style" href="tel:03134697631">(31) 3469
						7631</a> / <a class="no-style" href="tel:03134697622">3469 7622</a>
				</div>
			</div>
			<div class="about-info">
				<div class="title">PAC Jundiaí/SP</div>
				<br>
				<div class="description">
					Telefones: <a class="no-style" href="tel:01133082990">(11) 3308
						2990</a>
				</div>
			</div>
			<div class="about-info">
				<div class="title">PAC Curitiba/PR</div>
				<br>
				<div class="description">
					Telefones: <a class="no-style" href="tel:04121092012">(41) 2109
						2012</a>
				</div>
			</div>
			<div class="about-info">
				<div class="description">
					<strong>Email</strong>: <a class="no-style"
						href="mailto:sac.cooperfemsa@kof.com.mx">sac.cooperfemsa@kof.com.mx</a>
				</div>
				<br>
				<div class="description">
					<strong>0800 Ouvidoria</strong>: <a class="no-style"
						href="tel:08009409360">0800 940 93 60</a>
				</div>
				<br> <small>A Ouvidoria tem como finalidade representar o cooperado
					em defesa de seus interesses junto à Cooperativa. Para recorrer a
					Ouvidoria, será necessário ter sua solicitação e/ou reclamação
					analisada anteriormente pelos atendentes da cooperativa e que não
					tenha tido uma solução satisfatória.</small>
			</div>
		</div>

	</div>
</footer>
</div>


<!-- Included JS Files (Compressed) -->
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
<script
	src="<?php bloginfo('template_directory'); ?>/js/foundation.min.js"></script>

<!-- Initialize JS Plugins -->
<script src="<?php bloginfo('template_directory'); ?>/js/app.js"></script>

<script src="<?php bloginfo('template_directory'); ?>/js/forms.js"></script>


<script>
	var flyoutOpenedByClick = false;

	$(document).ready(function (e) {
		//
		// MENU
		//
		var flyoutList = document.getElementById('menu-menu-principal').querySelectorAll('.flyout');
		if (flyoutList.length > 0) {
			// for each ul.flyout, find all li child
			for (var i in flyoutList) {
				if (i >= 0) {
					var currentFlyout = flyoutList[i];

					// get all li
					var liList = currentFlyout.querySelectorAll('li');

					// for each li, find all has-flyout li
					for (var k in liList) {
						if (k >= 0) {
							var currentLi = liList[k];
							var nextLi = currentLi.nextSibling;
							var previousLi = currentLi.previousSibling;

							// verify if has flyout
							if (currentLi.classList.contains("has-flyout")) {

								previousLi = previousLi.previousSibling;

								// insert divisor before
								if (typeof(previousLi) != "undefined") {
									console.log(previousLi);
									if (typeof(previousLi.classList) == "undefined") {
										console.log(previousLi.classList);
										var newLiDivisor = document.createElement("LI");
										var newTextDivisor = document.createTextNode("-----------------------------");
										newLiDivisor.appendChild(newTextDivisor);
										newLiDivisor.classList.add('divisor');
										currentFlyout.insertBefore(newLiDivisor, currentLi);
									}
									else if (!previousLi.classList.contains('divisor')) {
										var newLiDivisor = document.createElement("LI");
										var newTextDivisor = document.createTextNode("-----------------------------");
										newLiDivisor.appendChild(newTextDivisor);
										newLiDivisor.classList.add('divisor');
										currentFlyout.insertBefore(newLiDivisor, currentLi);
									}
								}

								// get ul
								var subLiList = currentLi.querySelectorAll('ul.flyout li');

								// for each li child, get content and add after current LI
								for (var j in subLiList) {
									if (j >= 0) {
										var currentSubLi = subLiList[j];

										currentFlyout.insertBefore(currentSubLi, nextLi);
									}
								}

								// insert divisor after
								if (typeof(nextLi) != "undefined") {
									if (typeof(nextLi.classList) == "undefined") {
										var newLiDivisor = document.createElement("LI");
										var newTextDivisor = document.createTextNode("-----------------------------");
										newLiDivisor.appendChild(newTextDivisor);
										newLiDivisor.classList.add('divisor');
										currentFlyout.insertBefore(newLiDivisor, nextLi);
									}
									else if (!nextLi.classList.contains('divisor')) {
										var newLiDivisor = document.createElement("LI");
										var newTextDivisor = document.createTextNode("-----------------------------");
										newLiDivisor.appendChild(newTextDivisor);
										newLiDivisor.classList.add('divisor');
										currentFlyout.insertBefore(newLiDivisor, nextLi);
									}
								}

								// remove ul node
								currentLi.removeChild(currentLi.querySelector('ul'));

								// remove has-flyout class
								currentLi.classList.remove('has-flyout');
							}
						}
					}
				}
			}
		}
		
		//
		// SLIDER
		//
		if (slidersArray.length > 0) {
			var configs = {
					animation: "horizontal-slide",
					timer: true,
					resetTimerOnClick: true,
					pauseOnHover: true,
					startClockOnMouseOut: true,
					directionalNav: true,
					bullets: false,
					fluid: true,
			};

			// foreach slider, init orbit
			for (var i = 0; i < slidersArray.length; i++) {
				var id = "#" + slidersArray[i];
				$(id).orbit(configs);
			}
		}

		//
		// FORCE SHOW SUBMENUS
		//
		$('.has-flyout').on('mouseover mouseenter', function (e) {
// 			$(this).find('.flyout').show();
// 			$(this).closest('.flyout').show();

			var flyoutList = this.querySelectorAll('.flyout');
			if (flyoutList.length > 0) {
				flyoutList[0].style.display = "block"; // shows only the first
			}
		});
		$('.has-flyout').click(function (e) {
// 			$(this).find('.flyout').show();
// 			$(this).closest('.flyout').show();
			
			var flyoutList = this.querySelectorAll('.flyout');
			if (flyoutList.length > 0) {
				flyoutList[0].style.display = "block"; // shows only the first
			}
			
			flyoutOpenedByClick = true;
		});

		//
		// FORCE HIDE SUBMENUS
		//
		$('.has-flyout').on('mouseout mouseleave', function (e) {
// 			$(this).find('.flyout').hide();
// 			$(this).closest('.flyout').hide();
			var flyoutList = this.querySelectorAll('.flyout');
			if (flyoutList.length > 0) {
				flyoutList[0].style.display = "none";
			}
		});
		
		//
		// HIDE SUBMENU WHEN CLICK OUTSIDE
		//
		$(document).mouseup(function (e) {
			if (flyoutOpenedByClick) {
				$('.flyout').hide();
				flyoutOpenedByClick = false;
			}
		});
	});
	</script>

<?php wp_footer(); // get wordpress footer ?>
</body>
</html>
